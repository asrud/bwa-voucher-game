export interface CategoryTypes {
  _id: string;
  name: string;
  __v: number;
}

export interface GameItemTypes {
  _id: string;
  status: string;
  name: string;
  thumbnail: string;
  category: CategoryTypes;
}

export interface BanksTypes {
  _id: string;
  name: string;
  bankName: string;
  noAccount: number;
}

export interface PaymentTypes {
  _id: string;
  type: string;
  status: string;
  banks: BanksTypes[];
}

export interface NominalsTypes {
  _id: string;
  cointQty: number;
  cointName: string;
  price: number;
}

export interface LoginTypes {
  email: string;
  password: string;
}

export interface UserTypes {
  avatar: string;
  email: string;
  id: string;
  name: string;
  username: string;
}

export interface JWTPayload {
  player: UserTypes;
  iat: number;
}

export interface CheckoutTypes {
  voucher: string;
  nominal: string;
  payment: string;
  bank: string;
  name: string;
  accountUser: string;
}

export interface HistoryVoucherTopUpTypes {
  category: string;
  cointName: string;
  cointQty: number;
  gameName: string;
  price: number;
  thumbnail: string;
}

export interface HistoryPaymentsTypes extends BanksTypes {
  type: string;
}

export interface HistoryTransactionTypes {
  _id: string;
  historyVoucherTopUp: HistoryVoucherTopUpTypes;
  value: number;
  status: string;
  accountUser: string;
  tax: number;
  name: string;
  historyPayment: HistoryPaymentsTypes;
}

export interface TopUpCategoriesTypes {
  _id: string;
  value: number;
  name: string;
}
