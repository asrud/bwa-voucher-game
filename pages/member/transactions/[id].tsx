import TransactionDetailContent from '../../../components/organisms/TransactionDetailContent';
import { HistoryTransactionTypes } from '../../../services/data-types';
import { getTransactionDetail } from '../../../services/player';

interface TransactionsDetailProps {
  TransactionDetail: HistoryTransactionTypes;
}

export default function transactionDetail(props: TransactionsDetailProps) {
  const { TransactionDetail } = props;
  return (
    <section className="transactions-detail overflow-auto">
      <TransactionDetailContent data={TransactionDetail} />
    </section>
  );
}

interface GetServerSidePropsTypes {
  req: {
    cookies: {
      token: string;
    };
  };
  params: {
    id: string;
  };
}

export async function getServerSideProps({
  req,
  params,
}: GetServerSidePropsTypes) {
  const { token } = req.cookies;
  const { id } = params;
  if (!token) {
    return {
      redirect: {
        destination: '/sign-in',
        permanent: false,
      },
    };
  }
  const jwtToken = Buffer.from(token, 'base64').toString('ascii');

  const response = await getTransactionDetail(id, jwtToken);

  return {
    props: {
      TransactionDetail: response.data,
    },
  };
}
