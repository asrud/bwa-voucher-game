import jwtDecode from 'jwt-decode';
import { useEffect, useState } from 'react';
import Cookies from 'js-cookie';
import { JWTPayload, UserTypes } from '../../../services/data-types';

export default function Profile() {
  const [user, setUser] = useState({
    avatar: '',
    username: '',
    email: '',
  });

  useEffect(() => {
    const token = Cookies.get('token');
    if (token) {
      const jwtToken = atob(token);
      const payload: JWTPayload = jwtDecode(jwtToken);
      const userData: UserTypes = payload.player;
      const IMG = process.env.NEXT_PUBLIC_IMG;
      userData.avatar = `${IMG}/${userData.avatar}`;
      setUser(userData);
    }
  }, []);

  return (
    <>
      <div className="user text-center pb-50 pe-30">
        <img
          src={user.avatar}
          width="90"
          height="90"
          className="mb-20 rounded-circle"
          alt="user avatar"
        />
        <h2 className="fw-bold text-xl color-palette-1 m-0">{user.username}</h2>
        <p className="color-palette-2 m-0">{user.email}</p>
      </div>
    </>
  );
}
